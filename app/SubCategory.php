<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable =['sub-category_name'];

    public function products(){
        return $this->belongsToMany(Product::class);
    }
}
