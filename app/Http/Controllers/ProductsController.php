<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request){
        $allPorduct = Product::all();
        return response(['products'=>$allPorduct]);
    }
    public function store(Request $request){
        $validateRequest =$request->validate([
            'product_name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'category_id' => 'required',
            'thumbnail_image' =>'image|required'
        ]);
        $thumbnail_image = $validateRequest['thumbnail_image']->store('post');
        if($request->product_image-1) {
            $productImage1 = $request['product_image-1']->store('post');
        }
        if($request->product_image-2) {
            $productImage2 = $request['product_image-2']->store('post');
        }
        if($request->product_image-3) {
            $productImage3 = $request['product_image-3']->store('post');
        }
        if($request->product_image-4) {
            $productImage4 = $request['product_image-4']->store('post');
        }
        $product = Product::create([
            'product_name'=>$validateRequest['product_name'],
            'description'=>$validateRequest['description'],
            'price'=>$validateRequest['price'],
            'category_id' => $validateRequest['category_id'],
            'thumbnail_image' => $thumbnail_image,
            'product_image-1' => $productImage1,
            'product_image-2' => $productImage2,
            'product_image-3' => $productImage3,
            'product_image-4' => $productImage4
        ]);
        if ($request['subcategory_id']){
            $product->subcategories()->attach($request->input('subcategory_id'));
        }
        return response(['products'=>$product]);
    }
    public function delete(Product $product){
        $product->delete();
    }
}
