<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index(Request $request){
        $allCategory = Category::all();
        return response(['categories'=>$allCategory]);
    }
    public function store(Request $request){
        $validateCategory = $request->validate([
            'category_name'=>'required|unique:categories',
            'category_slug'=>'required',
        ]);
        $category = Category::create($validateCategory);
        return response(['category'=>$category]);
    }
}
