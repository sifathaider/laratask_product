<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        'category_id','subcategory_id','description','product_name','price','thumbnail_image','product_image-1',
        'product_image-2','product_image-3','product_image-4'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function subcategories(){
        return $this->belongsToMany(SubCategory::class);
    }
}
