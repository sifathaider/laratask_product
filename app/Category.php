<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'category_id','category_name','category_slug'
    ];
    public function products(){
        return $this->hasMany(Product::class);
    }
}
