<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register','AuthController@register');
Route::post('/login','AuthController@login');

Route::get('/categories','CategoriesController@index');
Route::post('/categories','CategoriesController@store');

Route::get('/sub-categories','SubCategoriesController@index');
Route::post('/sub-categories','SubCategoriesController@store');

Route::get('/products','ProductsController@index');
Route::post('/products','ProductsController@store');
Route::delete('/products/{id}','ProductsController@delete');
